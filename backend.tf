
terraform {

  backend "s3" {
  bucket = "stateful-bucket"
  dynamodb_table = "terraform-state-block"
  region = "us-east-1"
  key = "path/env"
  encrypt = true
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
  }
}
}